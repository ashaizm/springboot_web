package com.ptberry.web.api.out.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

public class WebLoginProcess extends WebAbstractLoginProcess{


	@Override
	public boolean onLoginSuccess(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		return super.onLoginSuccess(request, response);
	}

	@Override
	public boolean onLoginSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) {
		// TODO Auto-generated method stub
		return super.onLoginSuccess(request, response, authentication);
	}

	@Override
	public boolean onLoginFail(HttpServletRequest request, HttpServletResponse response, Exception exception) {
		// TODO Auto-generated method stub
		return super.onLoginFail(request, response, exception);
	}

	@Override
	public boolean onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) {
		// TODO Auto-generated method stub
		return super.onLogoutSuccess(request, response, authentication);
	}

	@Override
	public UserDetails onCustomLoginProcess(String userName, String password) {
		// TODO Auto-generated method stub
		return super.onCustomLoginProcess(userName, password);
	}

}
