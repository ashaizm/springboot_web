package com.ptberry.web.api.out.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

public interface WebInterfaceLoginProcess {
	boolean onLoginSuccess(HttpServletRequest request, HttpServletResponse response);
	boolean onLoginSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication);
	boolean onLoginFail(HttpServletRequest request, HttpServletResponse response, Exception exception);
	boolean onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication);
	boolean onAuthException(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException);
	UserDetails onCustomLoginProcess(String userName, String password);
}
