package com.ptberry.web.api.out.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

public abstract class WebAbstractLoginProcess implements WebInterfaceLoginProcess{

	@Override
	public boolean onLoginSuccess(HttpServletRequest request, HttpServletResponse response) {
		return false;
	}

	@Override
	public boolean onLoginSuccess(HttpServletRequest request, HttpServletResponse response,	Authentication authentication) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onLoginFail(HttpServletRequest request, HttpServletResponse response, Exception exception) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean onAuthException(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public UserDetails onCustomLoginProcess(String userName, String password) {
		// TODO Auto-generated method stub
		return null;
	}

}
