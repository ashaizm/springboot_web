package com.ptberry.web.api.out.login;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.authentication.ProviderManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.userdetails.UserDetailsServiceConfigurer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController{

	@RequestMapping(value = "/")   
	public String getLoginPage() {
		return "loginForm";
		//return "login/login";
	}	
	
	/*
	@RequestMapping(value = "/login/process", method = RequestMethod.POST) 
	public ModelAndView getLoginProgress(ModelAndView mav,@ModelAttribute Member member) {
		LoginUser lu = (LoginUser) memberService.findOne(member);
		mav.setViewName("main/main");
		return mav;
	}	
	
	@RequestMapping(value = "/main", method = RequestMethod.POST) 
	public ModelAndView getMainPage(ModelAndView mav,@ModelAttribute Member member) {
		LoginUser lu = (LoginUser) memberService.findOne(member);
		mav.setViewName("main/main");
		return mav;
	}		
	*/
}
