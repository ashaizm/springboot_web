package com.ptberry.web.api.config.security;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.ptberry.web.api.config.handler.LoginFailHandler;
import com.ptberry.web.api.config.handler.LoginSuccessHandler;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		http.headers().frameOptions().disable();    	
    	http.authorizeRequests()
    			.antMatchers("/**").permitAll()
    			.anyRequest().authenticated();
    	http.formLogin()
    		.loginPage("/loginForm")
    		.loginProcessingUrl("/login")
    		.defaultSuccessUrl("/main")
    		.successHandler(new LoginSuccessHandler())
    		.failureHandler(new LoginFailHandler())
    		.and().logout();
	}
}
